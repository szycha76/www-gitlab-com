require 'spec_helper'

describe Gitlab::CodeOwners do
  describe '.load_codeowners_file' do
    subject { described_class.load_codeowners_file('.') }

    context 'when file exists' do
      before do
        allow(::File).to receive(:exist?) { true }
        allow(::File).to receive(:read) { 'content' }
      end

      it 'returns CodeOwners::File' do
        is_expected.to be_a(Gitlab::CodeOwners::File)
      end
    end

    context 'when file is missing' do
      before do
        allow(::File).to receive(:exist?) { false }
      end

      it 'raises an exception' do
        expect { subject }.to raise_error('CODEOWNERS file is missing')
      end
    end
  end
end
