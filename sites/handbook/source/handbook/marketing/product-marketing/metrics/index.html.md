---
layout: handbook-page-toc
title: "Strategic Marketing Metrics"
---


## Content Produced

* [Strategic Marketing Content Inventory Issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/3353)

* [MVC1 on Google Sheets](https://docs.google.com/spreadsheets/d/1W5oAlbPV610-ylM7LWv_zc6bEqQK9dI0H9Hrn2f6Jwc/edit#gid=0)


## Web Traffic Analysis

* [Website and handbook pages](https://datastudio.google.com/u/0/reporting/1jhpxOcfWp9B44smdc6Uv-tGMNyWoludX/page/JKsTB)
* [YouTube Metrics](https://studio.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5AC)

* Learning Links:
    * [Shane Rice - youtube video on how to build such a dashboard](https://youtu.be/tNOD5qrH6Ao)
    * [Google Analytics for Beginners](https://analytics.google.com/analytics/academy/course/6)


## Marketing Attribution Model

* [Marketing attribution](https://app.periscopedata.com/app/gitlab/556414/Marketing-Linear-Attribution)

* Learning: [Marketing attribution - quick overview](https://www.bizible.com/blog/marketing-attribution-models-complete-list)


## Customer Reference Analytics

* [Customer Reference Edge Program](https://gitlab.my.salesforce.com/01Z4M000000slL1)
* [Monthly Marketing Key Metrics Slides](https://docs.google.com/presentation/d/1BGcMMqNfayuGsJVjqFCOXPrt5VK8S-KwHQiGr3d5KII/edit)

## Competitive Intelligence Analytics



## Examples to consider

* [ChangeLog](https://about.gitlab.com/handbook/CHANGELOG.html)
* Surveys (Sales team)
* Analytics on issues managed - open/closed, internal vs. external
* Pathfactory
* SM Issues Analysis
* SM Budget Spend analysis
* CAB Data (to consider)
    * x% that have case studies
    * y% are active in sales
    * z% are speaking in events (gitlab, industry, etc) for/with GitLab
    * a% are active in analyst reference calls
    * b% provide messaging, positioning, and persona feedback
    * c% “decline” for our requests?
    * STRETCH Goal:  Can we analyze if we have been able to sell more into these organizations since they joined the CAB




## CMO Challenge on how to measure

* Website first (MRs?)
* Metrics on views, etc.
* Maybe update BOM colors/links at every stand up


## Useful links

* [Marketing KPIs](https://about.gitlab.com/handbook/business-ops/data-team/kpi-index/#marketing-kpis)
* Slack Channel - #keymonthlymarketingmetric
* [Tech Stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0)
    * [Pathfactory](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/)
