---
layout: markdown_page
title: "Inactive workflows"
---

## Overview

The following response channel workflows are marked as inactive, as we either do not yet have a process in place or the capacity to effectively address support questions, mentions or comments there. The ultimate goal is to

## Workflows

### mentions-of-gitlab Slack channel

The [#mentions-of-gitlab](https://gitlab.slack.com/messages/mentions-of-gitlab/) chat channel tracks mentions of GitLab across multiple sources. This allows us to respond to user requests across various platforms.

We currently track the following sources for GitLab mentions:

1. Product Hunt
2. Hacker News
3. Reddit
4. YouTube
5. Quora

These mentions are piped by [notify.ly](https://notify.ly).

All comments on our [blog posts](/blog/) and any mention of GitLab on [Lobsters](https://lobste.rs/) also gets funneled to this channel using [Zapier](https://zapier.com).

#### Reddit

Respond to mentions of GitLab on Reddit, especially ones in the [GitLab Subreddit](https://www.reddit.com/r/gitlab/).

#### YouTube

Repond to comments made on the [GitLab Youtube Channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg).

#### Quora

Respond to questions about GitLab on Quora, especially the ones that appear in the [GitLab Topic channel](https://www.quora.com/topic/GitLab/).

### #movingtogitlab initiative

We have a daily reminder in the [#community-alerts](https://gitlab.slack.com/messages/community-alerts) Slack channel to check for new `#movingtogitlab` tweets.
This is done through TweetDeck, and the goal is to keep the `#movingtogitlab` trend and hashtag alive by retweeting appropriate tweets from our [movingtogitlab Twitter account](https://twitter.com/movingtogitlab).

When you retweet every relevant post, leave a checkmark on the reminder, so the rest of the team knows that it's done.

### Docs comments

These questions tend to be the most technical ones; consider involving experts when responding to them. Every comment should be answered.

Consider deleting the ones that aren't related to the documentation feedback. This kind of comments are distracting and aren't helpful to other users.

Types of comments that should be deleted:
  * Not documentation related
  * Support type issue
  * Feature Proposal
  * Outdated (deprecated by documentation changes)

_Warning_:  If done poorly, it can cause more damage than good. Please consider these steps:

1. Respond to the user (he gets an email with your response):
  * Sample response

  ```
  It looks like this issue is beyond the scope of the documentation comments. Please consider using <a href="https://forum.gitlab.com">our community forum</a>, or see <a href="https://about.gitlab.com/getting-help">other ways to get help</a>.
  Read more on how we handle documentation comments <a href="https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/#docs-comments">in our handbook</a>.
  Thanks for using GitLab!
  ```

2. Make sure the response contains a link to our [documentation process](/handbook/marketing/community-relations/community-advocacy/#docs-comments)
3. Delete the comment

### Mailing List

Respond to questions on the [GitLab Mailing List](https://groups.google.com/forum/#!forum/gitlabhq).

### GitLab Forum

Questions from the [GitLab Forum](https://forum.gitlab.com/) flow into ZenDesk, but can only be responded to from within the Forum environment.

### Stack Overflow

The [Stack Overflow tagged questions](https://stackoverflow.com/questions/tagged/gitlab) that relate to GitLab flow into Zendesk, but can only be responded to from within Stack Overflow.

After you create an account on [Stack Overflow](http://stackoverflow.com/) (if you don't already have one), you should start by answering a few simple questions in an area you're familiar with (a language, web framework, development platform, API, etc.) or in the GitLab tag(s) if you feel comfortable. The goal is to get enough ["Reputation"](http://stackoverflow.com/help/whats-reputation) and have access to a few more features.

Consider offering some of your Reputation using [bounties](http://stackoverflow.com/help/bounty) if a question is particularly advanced and you don't believe you can answer yourself, and the question seems deserving of an answer (e.g. if it has lots of upvotes).
