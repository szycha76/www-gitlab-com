---
layout: markdown_page
title: "Mike Terhar's README"
job: "Technical Account Manager - Federal"
---

## Mike Terhar README

**I'm Mike and I'm a [Federal Technical Account Manager](job-families/sales/technical-account-manager/).** This page is intended to help others understand what it should be like to work with me, especially people who can't figure out what I'm doing. Some of this is aspirational so please call me out any time I don't live up to my own values.
 
Please feel free to contribute to this page by opening a merge request. 

## About me

My background is a "de facto IT guy" who got started in the 1990s just to play Doom against my friends. Now, 30 years later, I have been a system administrator, programmer, project manager, strategist, and application security professional. You may notice that those are the skills that various stakeholders who would interact with GitLab-the-product. I think this makes my TAMing exceptionally effective at meeting the users where they are and enabling them... though this means a lot of the regular TAM activities are tough for me to keep up with.

*  Classic nerd from the 90s
*  Father of 3 kids, super proud of the little people
*  Can have incredibly dry, nearly caustic sense of humor 
*  Spent many years being outwardly deferential, quietly watching my bosses and teams struggle, with a smug feeling of superiority because _my way that I didn't share would have been so much better_. This 2020 form of me is the backlash against that guy. 

That last bullet is one of the reasons I love GitLab. I've also been exposed to psychological safety in other formats, some more aggressive than others. I believe GitLab's balance of polite and candid is the best mixture to the type of work we do. 

I attribute a sizable amount of success to people feeling comfortable enough to voice concerns or point out perceived deficiencies, then the DRI to select a course and just do the thing. The mixture of listening and bias-for-action moves us forward in a good direction. The inclusivity and diversity to bring in fresh and different ideas allows that listening and bias to have a better idea funnel. It's all tied together, and it's beautiful when it works.

## How you can help me

*  Push back when I make a claim that doesn't sound right.
*  Remind me about my job responsibilities, I will default to building a technical solution to solve a customer's problem rather than scheduling an EBR.
*  Point out when my opinions are unwanted or unhelpful so I can identify when to quiet down or at least who to engage with less directly.
*  Hit me up on Slack with feedback. I won't turn it into a conversation if you say "just fyi" at the beginning. 
*  Ask challenging clarifying questions when something doesn't sound right. I misread and misunderstand questions. 
*  Tell me about your experiences related to whatever is going on. Stories of success and failure and boredom and insanity are all welcome. 

## My working style

This section should probably be two sections. One about how I want to work and the other filled out by folks who actually work with me to say how it really is. Then I'll have some goals. 

#### Aspirational working style 

*  INTJ though weak in most of the measures
*  Meetings with fewer than 4 people should be discussions where we test ideas on each other
*  All discussions and threads revolve around what is right, not who is right (says the guy who shows up with the worst ideas sometimes) 
*  Effective hours are all over the map, so please don't respond to me outside of your working hours
*  I will *never* expect an urgent response out of your working hours but I may ask just in case you're available and can chip in
*  Make me aware of stuff outside my normal working hours so I can chip in if I'm available
*  Nobody should ever raise their voice unless someone is in danger or it might be funny

#### Actual working style 

*  (assume) Bull in a china shop jumping in too quick on customer calls
*  (assume) Contributes unnecessarily spicy comments and opinions (which can be fun or awkward)
*  (assume) Presents relatively uninformed ideas as facts
*  (name) Some hot takes on what Mike did in a meeting or email thread or slack channel

Submit some MRs to add how I work. I'm not a code owner so anyone who is a maintainer on this repo can merge it in. Just let me know at some point so I can receive the feedback. 

## What I assume about others

*  Related to above, I'll chip in with stories about my experiences at times; if it's not welcome, cut me off - they can get long
*  Us folks at GitLab-the-company are here because we want to be and we all believe GitLab-the-product is pretty special and want it to be the best
*  Customers of GitLab could be using the free version or a slew of competitors (at the same time) but have chosen GitLab-the-paid-product because they think it's special and will help them succeed
*  Non-customers have their own ideal workflows that may or may not be compatible with the opinionated GitLab way of working, but far more are compatible than expect to be

## What I want to earn

*  Trust as a technical resource to be the person sought for tricky solutions
*  Trust as a professional with expertise in a variety of domains 
*  Trust that I will be appreciative when challenged (not confrontational)
*  Credibility, when I bring feedback to the Product team, that my feedback comes from customer experience through the prism of my broad experience and GitLab-the-product knowledge
*  Access to your candid feedback (for real, you random people reading this)

## Communicating with me

*  I manage my notifications carefully and will only let them through when I'm receptive to them... you can send me any type of message at any time and it can't bother me
*  Informal communication is always welcome, Slack thread and /zoom are best for impromptu discussions
*  Please only email me if it's also going to clients; email isn't my thing
*  Never call me unless you like leaving voicemails
*  Text me if we are meeting somewhere for a client meeting or happy hour
*  No need to discuss a meeting ahead of time; just send an invite for either coffee chats or meetings with agendas based on my calendar availability
*  Join meetings early and we can chat also; I miss the hallway discussions, and early meetings seem like a good way to get some of that

## Strengths/Weaknesses

*  Hungry for knowledge and methods to do things better
*  Learn the theory but validate and test, for example, a homelab running on Kubernetes
*  Listens through the question being asked, decomposed to form a view of why that question would be asked, attempt to answer the question that they're not aware that they should be asking
*  Mixing strengths and weaknesses in the same list
*  When people are on the wrong path and refuse to hear reason, it really gets me down
*  Low tolerance for payroll errors caused by unnecessary complexity 

## Related pages

*  [brownfield.dev](https://brownfield.dev) is my attempt at bridging the gaps between the GitLab of marketing lore and the GitLab in a large enterprise with legacy applications and classic workflows
*  [Customer Success TAM page](/handbook/customer-success/tam/) which says a lot of things about how I might engage with clients
*  [Short toes](/handbook/values/#short-toes) my favorite sub-value, which I think contributes the most to effective collaboration that gets the best results.
*  [LinkedIn profile](https://www.linkedin.com/in/mterhar/)
*  [@mterhar on GitLab](https://gitlab.com/mterhar)
